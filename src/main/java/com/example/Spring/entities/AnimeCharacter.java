package com.example.Spring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ANIMECHARACTER")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnimeCharacter implements Serializable {
    @Id
    @GeneratedValue
    private  long idAnime;
    private String animeName;
    private String category;
    private String legend;
    private String strength;
    @Lob
    private byte[] photo;
    private boolean shared;

    @Transient
    private Long idOwner;

    @ManyToOne
    @JoinColumn(name = "idUser")
    private User user;

    public void setIdAnime(long idAnime) {
        this.idAnime = idAnime;
    }

    public long getIdAnime() {
        return idAnime;
    }

    public String getAnimeName() {
        return animeName;
    }

    public String getCategory() {
        return category;
    }

    public String getStrength() {
        return strength;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public boolean isShared() {
        return shared;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setAnimeName(String animeName) {
        this.animeName = animeName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLegend() {
        return legend;
    }

    public Long getIdOwner() {
        return idOwner;
    }

    public void setLegend(String legend) {
        this.legend = legend;
    }

    public void setIdOwner(Long idOwner) {
        this.idOwner = idOwner;
    }
}
