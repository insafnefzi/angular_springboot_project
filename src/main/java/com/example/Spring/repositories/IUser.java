package com.example.Spring.repositories;

import com.example.Spring.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUser extends JpaRepository<User, Long> {
    User findByMailAndPassword(String mail , String password);
    User findByIdUser(Long idUser);
}
