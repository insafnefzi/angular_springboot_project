package com.example.Spring.repositories;

import com.example.Spring.entities.AnimeCharacter;
import com.example.Spring.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IAnimeCharacter extends JpaRepository<AnimeCharacter,Long> {
    List<AnimeCharacter> findByUserOrShared(User user, boolean shared);
    List<AnimeCharacter> findByUser(User user);

    List<AnimeCharacter> findBySharedAndUserNotLike(boolean shared, User user);
}
