package com.example.Spring.controllers;

import com.example.Spring.entities.AnimeCharacter;
import com.example.Spring.entities.User;
import com.example.Spring.repositories.IAnimeCharacter;
import com.example.Spring.repositories.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1/animes")
public class AnimeController  {
    @Autowired
    private IAnimeCharacter animeRepository;
    @Autowired
    private IUser userRepository;

    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(animeRepository.findAll());
    }

    @GetMapping("/all/{idUser}")
    public ResponseEntity findAllUserCharacters(@PathVariable(name="idUser") Long idUser) {
        if (idUser == null) {
            return ResponseEntity.badRequest().body("Cannot find anime with null user");
        }
        User user = userRepository.getOne(idUser);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        List<AnimeCharacter> userChars = animeRepository.findByUser(user);
        List<AnimeCharacter> sharedChars = animeRepository.findBySharedAndUserNotLike(true, user);
        userChars.forEach(character -> character.setIdOwner(idUser));
        sharedChars.forEach(character -> character.setIdOwner(-1L));
        userChars.addAll(sharedChars);

        return ResponseEntity.ok().body(animeRepository.findByUserOrShared(user,true));
    }

    @GetMapping("/{idAnime}")
    public ResponseEntity findUserById(@PathVariable(name="idAnime") Long idAnime){
        if (idAnime==null){
            return ResponseEntity.badRequest().body("cannot retrieve anime with null ID");
        }
        AnimeCharacter anime = animeRepository.getOne(idAnime);
        if (anime==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(anime);
    }

    @PostMapping("/")
    public  ResponseEntity createUser(@RequestBody AnimeCharacter anime){
        if (anime ==null){
            return ResponseEntity.badRequest().body("cannot create anime with empty fields");
        }
        AnimeCharacter createdAnime = animeRepository.save(anime);
        return ResponseEntity.ok(createdAnime);
    }
    @DeleteMapping("/(idAnime)")
    public ResponseEntity deleteAnime(@PathVariable(name="idAnime") Long idAnime){
        if (idAnime==null){
            return ResponseEntity.badRequest().body("cannot delete anime with null ID");
        }
        AnimeCharacter anime= animeRepository.getOne(idAnime);
        if (anime==null){
            return ResponseEntity.notFound().build();
        }
        animeRepository.delete(anime);
        return ResponseEntity.ok("anime deleted with success");
    }
    @GetMapping("/share/(idAnime)/(isShared)")
    public ResponseEntity shareAnime(@PathVariable(name = "idAnime")Long idAnime, @PathVariable(name = "isShared") boolean isShared){
        if (idAnime==null){
            return ResponseEntity.badRequest().body("cannot delete anime with null ID");
        }
        AnimeCharacter anime= animeRepository.getOne(idAnime);
        if (anime==null){
            return ResponseEntity.notFound().build();
        }
        anime.setShared(true);
            return ResponseEntity.ok(animeRepository.save(anime));
    }

}
