package com.example.Spring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

 @Entity
 @Table(name = "USER")
 @Data
 @NoArgsConstructor
 @AllArgsConstructor
 @Builder
public class User implements Serializable {
     @Id
     @GeneratedValue
     private  long idUser;
     private String firstName;
     private String lastName;
     private String mail;
     private String password;
     @Lob
     private byte[] photo;
     @OneToMany(mappedBy = "user")
    private List<AnimeCharacter> characters;



     public long getIdUser() {
         return idUser;
     }

     public String getFirstName() {
         return firstName;
     }

     public String getLastName() {
         return lastName;
     }

     public String getMail() {
         return mail;
     }

     public String getPassword() {
         return password;
     }

     public byte[] getPhoto() {
         return photo;
     }
    @JsonIgnore
     public List<AnimeCharacter> getCharacters() {
         return characters;
     }

     public void setIdUser(long idUser) {
         this.idUser = idUser;
     }

     public void setFirstName(String firstName) {
         this.firstName = firstName;
     }

     public void setLastName(String lastName) {
         this.lastName = lastName;
     }

     public void setMail(String mail) {
         this.mail = mail;
     }

     public void setPassword(String password) {
         this.password = password;
     }

     public void setPhoto(byte[] photo) {
         this.photo = photo;
     }

     public void setCharacters(List<AnimeCharacter> characters) {
         this.characters = characters;
     }
 }

